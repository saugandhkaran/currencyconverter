import { Component, OnInit } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { CurrencyService } from '../../services/currency.service';
import { CurrencyObject } from '../../models/currency-object';
import { ConversionHistoryService } from '../../services/conversion-history.service';
import { ExchangeRateHistoryModel } from '../../models/exchange-rate-history';

@Component({
  selector: 'app-currency-converter',
  templateUrl: './currency-converter.component.html',
  styleUrls: ['./currency-converter.component.scss']
})
export class CurrencyConverterComponent implements OnInit {

  constructor(private currencyService: CurrencyService,
    private convertionHistoryService: ConversionHistoryService,
    private route: ActivatedRoute,
    ) { }
  public options;
  public exchangeHistoryDuration = 7;
  public converted = false;
  public unitPriceTo;
  public unitPriceFrom;
  public exchangeRateHistory = [];
  public statisticsArray = [];
  public days = [{
    'days': 7,
    'label': '7 days'
  },
  {
    'days': 14,
    'label': '14 days'
  },
  {
    'days': 30,
    'label': '30 days'
  }];
  public currencyObject = new CurrencyObject;
  displayedColumns: string[] = ['date', 'event'];

  ngOnInit() {
    this.populateCurrencyTypes();
    this.route.params.subscribe(params => {
      if (JSON.stringify(params) !== '{}') {
        this.currencyObject.amount = params.amount;
        this.getAndSetCurrencyObject(params);
      }
    });
  }

  refreshExchangeHistory(event, currencyFrom, currencyTo): void {
    if (event.isUserInput) {
      this.getExchangeRateHistory(event.source.value, currencyFrom, currencyTo);
    } else {
      return;
    }
  }

  getAndSetCurrencyObject (params): void {
    this.currencyService.getAllCurrencyTypes().subscribe((data) => {
      this.options = data;
      this.currencyObject.amount = params.amount;
      this.currencyObject.from = this.options.find( x => x.currency === params.from);
      this.currencyObject.to = this.options.find( x => x.currency === params.to);
      this.convert();
    });
  }

  getExchangeRateHistory(days, currencyFrom, currencyTo): void {
    forkJoin(this.currencyService.getExchangeHistory(days, currencyFrom.currency),
    this.currencyService.getExchangeHistory(days, currencyTo.currency) ).subscribe((data) => {
      const currencyFromHistory: any = data[0];
      const currencyToHistory: any = data[1];
      this.exchangeRateHistory = [];
      for (let i = 0; i < currencyFromHistory.length; i++) {
        const a = new ExchangeRateHistoryModel;
        a.rate = this.conversionMechanism(currencyFromHistory[i].rate, currencyToHistory[i].rate);
        a.timestamp = currencyFromHistory[i].timestamp;
        this.exchangeRateHistory.push(a);
      }
      this.exchangeRateHistory.sort((a, b) => {
        const am: any = new Date(b.timestamp);
        const bm: any = new Date(a.timestamp);
        return am - bm;
      });
      this.statisticsArray = this.currencyService.getStatisticsArray(this.exchangeRateHistory);
      console.log(this.statisticsArray);
    }, (error) => {
      console.error('Error getting the exchange rate history');
    });
  }

  populateCurrencyTypes () {
    this.currencyService.getAllCurrencyTypes().subscribe((data) => {
      this.options = data;
      return this.options;
    }, (error) => {
      console.error('Error getting list of currency types!');
      return Observable.throw(error);
    });
  }

  switch(): void {
    this.converted = false;
    [this.currencyObject.to, this.currencyObject.from] = [this.currencyObject.from, this.currencyObject.to];
    [this.unitPriceFrom, this.unitPriceTo] = [this.unitPriceTo, this.unitPriceFrom];
  }

  conversionMechanism( to, from): number {
    return (to / from);
  }

  pushDataToStorage(): void {
    const currentDate = new Date();
    this.currencyObject.timeOfSearch = currentDate;
    this.convertionHistoryService.saveConversionHistory(this.currencyObject);
  }

  convert (): void {
    if (this.currencyObject.amount) {
    this.unitPriceTo = this.conversionMechanism(this.currencyObject.to.rate, this.currencyObject.from.rate);
    this.unitPriceFrom = this.conversionMechanism(this.currencyObject.from.rate, this.currencyObject.to.rate);
    this.getExchangeRateHistory( 7, this.currencyObject.from, this.currencyObject.to );
    this.converted = true;
    this.pushDataToStorage();
    }
  }

}
