import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../app/auth/auth.guard';
import { LoginComponent } from './components/login/login.component';
import { CurrencyConverterComponent } from './components/currency-converter/currency-converter.component';
import { ConverterHistoryComponent } from './components/converter-history/converter-history.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'currencyConverter', component: CurrencyConverterComponent, canActivate: [AuthGuard] },
  { path: 'converterHistory', component: ConverterHistoryComponent, canActivate: [AuthGuard] },
  { path: '',   redirectTo: '/login', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }
