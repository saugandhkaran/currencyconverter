import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService,
     private router: Router) { }

  public loginForm = new FormGroup({
    username : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required),
  });


  ngOnInit() {
    if (this.authenticationService.getToken()) {
      this.router.navigate(['/currencyConverter']);
    }

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  get f() { return this.loginForm.controls; }

  login(loginForm) {
    console.log(loginForm);
    if (this.loginForm.invalid) {
      return;
    } else {
      console.log(this.loginForm.value);
      if (this.authenticationService.validateLogin(this.loginForm.value)) {
        this.authenticationService.setCheckLoggedIn(true);
        this.authenticationService.sendToken('test');
        this.router.navigate(['currencyConverter']);
      }
    }
  }

}
