import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedIn;
  constructor(private authService: AuthenticationService) {
    this.loggedIn = this.authService.getCheckLoggedIn().subscribe((newValue) => {
      if (newValue) {
        this.loggedIn = true;
    } else {
        this.loggedIn = false;
    }
  });
  }

  ngOnInit() {
    this.loggedIn = this.authService.getToken();
  }

  logOut() {
    this.authService.setCheckLoggedIn(false);
    this.authService.logout();
  }

}
