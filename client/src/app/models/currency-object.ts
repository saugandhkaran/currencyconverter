export class CurrencyObject {
  amount: number;
  to: {
    rate: string,
    currency: string,
    timestamp: string
  };
  from: {
    rate: string,
    currency: string,
    timestamp: string
  };
  timeOfSearch = new Date();
}
