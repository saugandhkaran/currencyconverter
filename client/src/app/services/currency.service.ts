import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const header_object = new HttpHeaders();
header_object.append('Content-Type', 'application/json');
header_object.append('Key', '6e45a670a3d7dc3ff90ff29672a949a7');

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(private http: HttpClient) { }

  getAllCurrencyTypes() {
    return this.http.get('https://api.nomics.com/v1/exchange-rates?key=6e45a670a3d7dc3ff90ff29672a949a7', {headers : header_object});
  }

  getExchangeHistory(days, currency) {
    const endDate = new Date();
    const startDate = new Date();
    startDate.setDate(endDate.getDate() - days);
    const params_object = new HttpParams()
    .append('currency', currency)
    .append('end', endDate.toISOString().slice(0, -5) + 'Z')
    .append('start', startDate.toISOString().slice(0, -5) + 'Z');
    return this.http.get('https://api.nomics.com/v1/exchange-rates/history?key=6e45a670a3d7dc3ff90ff29672a949a7',
    {headers: header_object , params: params_object});
  }

  getStatisticsArray(exchangeRateArray) {
    const a = [];
    a.push(this.getMaximumValue(exchangeRateArray));
    a.push(this.getMinimumValue(exchangeRateArray));
    a.push(this.getAverageValue(exchangeRateArray));
    console.log(a);
    return a;
  }

  getMaximumValue(exchangeRateArray) {
    const maxObject = {
    'label' : 'Maximum',
    'rate' : exchangeRateArray.reduce((max, p) => p.rate > max ? p.rate : max, exchangeRateArray[0].rate)
    };
    return maxObject;
  }
  getMinimumValue(exchangeRateArray) {
    const minObject = {
    'label' : 'Minimum',
    'rate' : exchangeRateArray.reduce((min, p) => p.rate < min ? p.rate : min, exchangeRateArray[0].rate)
  };
    return minObject;
  }
  getAverageValue(exchangeRateArray) {
    let sum = 0 ;
    exchangeRateArray.forEach(element => {
      sum += element.rate;
    });
    const avgObject = {
      'label' : 'Average',
      'rate' : sum / exchangeRateArray.length
    };
    return avgObject;
  }
}
