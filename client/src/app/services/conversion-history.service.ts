import { Injectable } from '@angular/core';
import { CurrencyObject } from '../models/currency-object';

@Injectable({
  providedIn: 'root'
})
export class ConversionHistoryService {

  constructor() { }

  saveConversionHistory(convertObject: CurrencyObject) {
    if (!localStorage.getItem('converterHistory')) {
      const a = [];
      localStorage.setItem('converterHistory', JSON.stringify(a));
    } else {
      let a = [];
      a = JSON.parse(localStorage.getItem('converterHistory'));
      a.push(convertObject);
      localStorage.setItem('converterHistory', JSON.stringify(a));
    }
  }

  getConversionHistory() {
    let a = [];
    a = JSON.parse(localStorage.getItem('converterHistory'));
    return a;
  }

  deleteAnEntry(element) {
    let a = [];
    a = JSON.parse(localStorage.getItem('converterHistory'));
    a.splice(a.findIndex(item => item.timeOfSearch === element.timeOfSearch), 1);
    localStorage.setItem('converterHistory', JSON.stringify(a));
  }
}
