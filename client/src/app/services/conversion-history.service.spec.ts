import { TestBed } from '@angular/core/testing';

import { ConversionHistoryService } from './conversion-history.service';

describe('ConversionHistoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConversionHistoryService = TestBed.get(ConversionHistoryService);
    expect(service).toBeTruthy();
  });
});
