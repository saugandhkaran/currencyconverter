import { Component, OnInit } from '@angular/core';
import { ConversionHistoryService } from '../../services/conversion-history.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-converter-history',
  templateUrl: './converter-history.component.html',
  styleUrls: ['./converter-history.component.scss']
})
export class ConverterHistoryComponent implements OnInit {

  constructor(private conversionHistoryService: ConversionHistoryService, private router: Router) { }
  displayedColumns: string[] = ['date', 'event', 'actions'];
  onTheItem = false;
  dataSource = [];
  ngOnInit() {
    this.getAllConvertHistory();
  }

  getAllConvertHistory() {
    this.dataSource = this.conversionHistoryService.getConversionHistory();
  }

  deleteElement(element) {
    console.log(element);
    this.conversionHistoryService.deleteAnEntry(element);
    this.getAllConvertHistory();
  }

  viewConversion(element) {
    const params  = {
    'amount' : element.amount,
    'to' : element.to.currency,
    'from' : element.from.currency
    };
    this.router.navigate(['/currencyConverter', params]);
  }

}
