export class ExchangeRateHistoryModel {
  rate: number;
  timestamp: string;
}
