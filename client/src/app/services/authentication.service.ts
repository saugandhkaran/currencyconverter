
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private userDB = [
    { 'username': 'user1', 'password': 'pass1', 'fullName': 'John Doe' },
    { 'username': 'user2', 'password': 'pass2', 'fullName': 'Adam Smith' }
  ];

  private checkLoggedIn: Subject<boolean> = new Subject();

  public getCheckLoggedIn(): Observable<any> {
    return this.checkLoggedIn.asObservable();
  }

  public setCheckLoggedIn(value: boolean): void {
    this.checkLoggedIn.next(value);
}

  constructor(private router: Router) { }
  sendToken(token: string) {
    localStorage.setItem('LoggedInUser', token);
  }

  getToken() {
    return localStorage.getItem('LoggedInUser');
  }

  isLoggedIn() {
    return this.getToken() !== null;
  }

  validateLogin(parameters) {
    if ((this.userDB.filter(item => item.username === parameters.username && item.password === parameters.password)).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('LoggedInUser');
    this.router.navigate(['login']);
  }

}
